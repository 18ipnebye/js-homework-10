let getElementFooter = document.querySelector("footer")
let getElementMain = document.querySelector("main")

let newElementA = document.createElement('a')
newElementA.href = '#'
newElementA.textContent = "Learn More"
newElementA.style.fontSize = '20px'
newElementA.style.textDecoration = 'none'
newElementA.style.color = '#7d7777'
getElementFooter.append(newElementA)

let newElementSelect = document.createElement('select')
newElementSelect.classList.add('rating')
getElementMain.prepend(newElementSelect)



for(let el = 4; el >= 1; el--){
    let newElementOption = document.createElement('option');
    if (el === 1){
        newElementOption.textContent = (`${el} Star`);
    }else{
        newElementOption.textContent = (`${el} Stars`);
    }
    newElementOption.style.fontSize = '30px';
    newElementSelect.append(newElementOption)
}